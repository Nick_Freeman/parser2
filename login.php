<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require('connect_db.php');


function generateCode($length = 6)
{
    $chars = "abcdefghijkmnopqrstuvwxyzABCDEFGHIJKMNOPQRSTUVWXYZ0123456789";
    $code = '';
    $clen = strlen($chars) - 1;
    while (strlen($code) < $clen) {
        $code .= $chars[mt_rand(0, $clen)];
    }

    return $code;
}

if (isset($_POST['submit'])) {
    $select_user = 'select id, password from users where login=:login limit 1';
    $query = $pdo->prepare($select_user);

    $query->bindValue(':login', $_POST['login']);
    $query->execute();
    $data_user = $query->fetch();
    $role = $_POST['login'];
    if ($data_user['password'] === md5(md5($_POST['password']))) {
        $hash = md5(generateCode(10));

        $update_data = $pdo->prepare('update users set hash=:hash where id=:id');
        $update_data->bindValue(':hash', $hash);
        $update_data->bindValue(':id', $data_user['id']);
        $update_data->execute();

        setcookie('id', $data_user['id'], time() + 60 * 60 * 24 * 30, '/');
        setcookie('hash', $hash, time() + 60 * 60 * 24 * 30, '/', null, null, true);
        setcookie('role',$role,time() + 60 * 60 * 24 * 30, '/');


        if ($_POST['login'] == 'admin') {

            header('Location: adminIndex.php') ;
        } else {

            header('Location: index.php') ;
        }
        exit();
    } else {
        header('Location: loginError.php') ;
//        print 'Вы ввели не правильный логин.пароль';

    }


}
//return $output;
?>


