<?php
if (stripos($_SERVER['HTTP_REFERER'], 'parser.php') === false) ;
require_once('parser.php');
include('login.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
              integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
              crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>

        <script type="text/javascript" src="/js/newsOutput.js"></script>

    </head>
<body>
    <!--    check user role-->
<? if (!isset($_COOKIE['role'])):header('Location:login.html'); endif; ?>

<? if (isset($_COOKIE['role']) && $_COOKIE['role'] == 'admin'): ?>


    <nav class="navbar navbar-light bg-light">

        <div class="search">
            <form class="form-inline my-2 my-lg-0" action="/search.php" method="get">
                <input class="form-control mr-sm-2" type="search" placeholder="Search by title" name="search"
                       value="<?= isset($_GET['search']) ? $_GET['search'] : '' ?>" required>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
        <?php if (isset($_COOKIE['role'])): ?>
            <a class="btn btn-success"> Hi ,<?= $_COOKIE['role']; ?></a>
            <a class="btn btn-outline-info" href="parser.php?parserRedirect=1">Update news</a>
        <? endif; ?>

        <?php if (!isset($_COOKIE['role'])): ?>
            <form class="form-inline" action="login.html" method="post">
                <button class="btn btn-outline-success" value="login" name="login" type="submit">Login</button>
            </form>
            <form class="form-inline" action="register.html" method="post">
                <button class="btn btn-outline-warning" name="register" type="submit">Register</button>
            </form>

        <? endif; ?>
        <form class="form-inline" action="logout.php" method="post">
            <button class="btn btn-outline-danger" value="logout" name="logout" type="submit">Logout</button>
        </form>


    </nav>


    <!--output news-->
    <div class="parent" id="parent">

    </div>

    <div class="btnAdd">
        <input type="button" class="btn btn-success btn_marg" value="add news" onclick="add()">
    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
            integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
            crossorigin="anonymous"></script>
<!--JQuery CDN-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>


    </body>
    </html>

    <!--redirect-->
<? else: header('Location:index.php') ?>
<? endif; ?>