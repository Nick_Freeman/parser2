function getPost(cd, one) {

    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'newsToJson.php?id=' + one);
    xhr.addEventListener('readystatechange', () => {
        if (xhr.status == 200 && xhr.readyState == 4) {
            cd(JSON.parse(xhr.responseText));

        }
    });
    xhr.send();
}


let id = 0;

function add() {
    getPost(response => {
        let card_id = document.getElementById('parent');
        let card_i3 = document.createElement('div');
        card_i3.className = "news";


        response.news.forEach(post => {

            let card_i = document.createElement('div');
            card_i.className = "card mb-3";
            card_i.id = "card";

            let imgs = document.createElement('img');
            imgs.className = "card-img-top";
            imgs.src = post.img;

            let card_body = document.createElement('div');
            card_body.className = "card-body";

            let card_title = document.createElement('h5');
            card_title.className = "card-title";
            card_title.innerHTML = post.title;

            let p_data = document.createElement('p');
            p_data.className = "card-text";
            p_data.innerHTML = post.date;

            let news_id = document.createElement('input');
            news_id.type = "hidden";
            news_id.value = post.id;
            news_id.innerHTML = post.id;

            let butInfo = document.createElement('a');
            butInfo.className = "btn btn-info";
            butInfo.href = post.link;
            butInfo.innerHTML = 'More info';

            let butEdit = document.createElement('a');
            butEdit.className = "btn btn-success";
            butEdit.href = 'edits.php?id=' + post.id;
            butEdit.innerHTML = 'Edit';

            let butDelete = document.createElement('a');
            butDelete.className = "btn btn-warning";
            butDelete.id = "delete";
            butDelete.onclick = ()=>{deleteNewsLocale(post.id)};
            butDelete.innerHTML = 'Delete';


            card_body.appendChild(p_data);
            card_body.appendChild(card_title);
            card_body.appendChild(news_id);

            //button add
            card_body.appendChild(butInfo);

            if (response.role['role'] === 'admin') {
                card_body.appendChild(butEdit);
                card_body.appendChild(butDelete);
            }
            card_i.appendChild(imgs);
            card_i.appendChild(card_body);

            card_i3.appendChild(card_i);


        });

        card_id.appendChild(card_i3);

    }, id);
    id += 3;
}

add();



function deleteNewsLocale(id) {

    let parnt = document.getElementById('card');
    let delete_news = confirm('Delete news ???');
    if (delete_news === true) {

        let url = '/destroy.php?id=' + id;

        $.ajax({
            url: url,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {

                    if (data != null) {
                        parnt.remove()

                    }
            }
        });
    }
}



