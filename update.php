<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'connect_db.php';

//    header('Location:index.php');

if (!isset($_POST['id'])) {

    echo 'empty id';
}

$title = $_POST['title'];
$date = $_POST['date'];
$description = $_POST['description'];
$link = $_POST['link'];
$image = $_FILES['img']['tmp_name'];
$name = $_FILES['img']['name'];
$id = (int)$_POST['id'];

try {

    if (isset($image)) {
        $folder = 'imgs/';
        $img = $folder . $name;
        copy($image, $img);
    } else {
        $img = 'NULL';
    }

    $sql = 'UPDATE dataparser 
		SET title = :title,
		 date = :date,
		 description = :description,
		 link = :link,
         img = :img
		 WHERE id = :id
	';


    $smtp = $pdo->prepare($sql);
    $smtp->bindValue(':title', $title);
    $smtp->bindValue(':date', $date);
    $smtp->bindValue(':description', $description);
    $smtp->bindValue(':link', $link);
    $smtp->bindValue(':img', $img);
    $smtp->bindValue(':id', $id);

    $smtp->execute();


    header('Location:adminIndex.php');
    die();

} catch (Exception $e) {
    echo 'Error' . $e->getMessage();
    die();
}
