<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
set_time_limit(0);
require('connect_db.php');
require('send.php');

$str = file_get_contents('https://www.dailytargum.com/section/news');

$email = 'workes221@gmail.com';
$name = 'admin';


preg_match_all('#<article[^>]+?class\s*?=\s*?(["\'])art-left\1[^>]*?>(.*?)</article>#su', $str, $query);
foreach ($query[0] as $data) {
    preg_match_all('#<h2[^>]+?class\s*?=\s*?(["\’])headline has-photo\1[^>]*?>.*?<a[^>]+?>(.*?)</a>.*?</h2>#su', $data, $title);
    preg_match_all('#<span[^>]+?class\s*?=\s*?(["\'])time-since\1[^>]*?>(.*?)</span>#su', $data, $date);
    preg_match_all('#<p[^>]+?class\s*?=\s*?(["\'])article-abstract has-photo d-none d-md-block\1[^>]*?>(.*?)</p>#su', $data, $description);
    preg_match_all('#<a[^>]*?href\s*?=\s*?["\'](.+?)["\'].*?></a>#su', $data, $link);
    preg_match_all('#<img[^>]*?src\s*?=\s*?["\'](.+?)["\'].*?>#su', $data, $img);

    $text = $title[2][0];

    if (!check($pdo, $title[2][0])) {
        dataparser($pdo, $title, $date, $description, $link, $img);
        send($email,$name,$text);
    }
}

if (isset($_GET['parserRedirect'])) {
    header('Location: adminIndex.php');
}

function dataparser(PDO $pdo, $title, $date, $description, $link, $img)
{
    $sql_parser = 'insert into dataparser set
                    title=:title,
                    date=:date,
                    description=:description,
                    link=:link,
                    img=:img
                    ';

    if (isset($img[1][0])) {
        preg_match_all('#([^/]+)\.#su', $img[1][0], $name);

        $folder = 'imgs/';
        $source = $img[1][0];
        $dest = $folder . $name[1][1] . '.png';
        copy($source, $dest);

    } else {
        $dest = 'NULL';
    }

    $query = $pdo->prepare($sql_parser);
    $query->bindValue(':title', $title[2][0]);
    $query->bindValue(':date', $date[2][0]);
    $query->bindValue(':description', $description[2][0]);
    $query->bindValue(':link', $link[1][0]);
    $query->bindValue(':img', $dest);

    $query->execute();


}

function check(PDO $db, $head)
{
    $f = $db->prepare('select title from dataparser where title like :headline');
    $f->bindValue(':headline', $head);
    $f->execute();
    return $f->fetch();
}
//echo "that's work";