<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$pdo = new PDO('mysql:host=localhost;dbname=parser', '2', 'password');

$sql = 'create table if not exists dataparser (
            id int not null auto_increment primary key,
            title varchar(200),
            date varchar(200),
            description varchar(350),
            link varchar(200),
            img varchar(200) 
            ); ';

$sql2 = 'create table if not exists users (
            id int not null auto_increment primary key,
            login varchar(30) UNIQUE,
            password varchar(32),
            hash varchar(32),
            ip int(10)
            );';

$pdo->query($sql);
$pdo->query($sql2);

//$password = md5(md5('admin'));
//$data = 'insert into users set
//            login="admin",
//            password="'.$password.'"
//            ';
//$pdo->query($data);

$select_user = 'select * from users';
$users = $pdo->query($select_user)->fetchAll();

$select = 'select * from dataparser';
$select_data = $pdo->query($select)->fetchAll();

$getTitles = 'select title from dataparser';
$outputTitles = $pdo->query($getTitles)->fetchAll();