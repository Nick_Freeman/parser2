<?php
require('connect_db.php');
try {
    $id = (int)$_GET['id'];
    $sql = "SELECT * FROM dataparser WHERE id = :id";
    $x = $pdo->prepare($sql);
    $x->bindValue(':id', $id);
    $x->execute();
    $quote = $x->fetch();
} catch (Exception $e) {
    echo 'Error' . $e->getMessage();
    die();
}


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>

<nav class="navbar navbar-light bg-light">
    <a class="btn btn-outline-info" href="adminIndex.php">Return back</a>
</nav>

<form class="form-group" action='update.php' method="post" enctype="multipart/form-data">


        <div class="edit">

            <label class="form-control" for='title'>Enter new title:</label>
            <input class="editTitle" name='title' id="title" value="<?= $quote['title'] ?>">
            <br>
            <br>
            <label class="form-control" for="date">Enter new date:</label>
            <input class="editDate"   name="date" id="date" value="<?= $quote['date'] ?>">
            <br>
            <br>
            <label class="form-control" for="description">Enter new description:</label>
            <input  class="editDesc"  name="description" id='description' value="<?= $quote['description'] ?>">
            <br>
            <br>
            <label class="form-control" for='link'>Enter new link:</label>
            <input class="editLink"  name='link' id="link" value="<?= $quote['link'] ?>">
            <br>
            <br>
            <img class="imgEdit" id="img" src="<?= $quote['img'];?>">
            <label class="form-group" for="img">Enter new image:</label>
            <input class="form-control" id="" onchange="previewFile()" name="img" type="file"/>
            <br>
            <input class="form-control" type="hidden" name="id" value="<?= $_GET['id']; ?>">

            <button class="btn btn-info" type='submit'>Save</button>
        </div>


</form>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="/js/substitution.js"></script>
</body>
</html>

