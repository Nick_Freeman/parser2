<?php
require_once('parser.php');
include('login.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>

<nav class="navbar navbar-light bg-light">
    <? if (!isset($_COOKIE['role'])):header('Location:login.html'); endif; ?>
    <?php if (isset($_COOKIE['role'])): ?>
        <?php if ($_COOKIE['role'] == 'admin'):header('Location:adminIndex.php'); endif; ?>

        <a class="btn btn-success"> Hi ,<?= $_COOKIE['role']; ?></a>
    <? endif; ?>


    <?php if (!isset($_COOKIE['role'])): ?>
        <form class="form-inline" action="register.html" method="post">
            <button class="btn btn-outline-warning" name="register" type="submit">Register</button>
        </form>
    <? endif; ?>
    <form class="form-inline" action="logout.php" method="post">
        <button class="btn btn-outline-danger" value="logout" name="logout" type="submit">Logout</button>
    </form>

</nav>

<div class="parent" id="parent"></div>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
                integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
                crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
                integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
                crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.js"
                integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
                crossorigin="anonymous"></script>
        <script type="text/javascript" src="/js/newsOutput.js"></script>
</body>
</html>

